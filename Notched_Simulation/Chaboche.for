*****************************************************************top***
**: <Fully implicit integration of the Revised Chaboche model with static recovery 
**：Kinematic hardening owns static recovery
**: 各向同性强化包含静态恢复
**:  note : consistent tangent modulus needs to be revised later !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
**:  for 3D solid elements>
************************************************************************
**:  Developed by Rou DU at 2022.10.31
**:
************************************************************************
**:  Notice!
**:
**:  Set '*depvar'=ntens*(mah+1)+1 in input file.
**:
************************************************************************
**: <ABAQUS user subroutine, UMAT>
************************************************************************
**:
**: <UMAT heading>
**:
      subroutine umat(ss,statev,ddsdde,sse,spd,scd,
     & rpl,ddsddt,drplde,drpldt,
     & sn,dsn,time,dtime,temp,dtemp,predef,dpred,cmname,
     & ndi,nshr,ntens,nstatv,props,nprops,coords,drot,pnewdt,
     & celent,dfgrd0,dfgrd1,noel,npt,layer,kspt,kstep,kinc)

c      include 'aba_param.inc'     
	  implicit none 	  
	  
**: declare interface variables	  
	  integer ndi,nshr,ntens,nstatv,nprops
	  integer noel,npt,layer,kspt,kstep,kinc
      character*8 cmname
      real(8) statev(nstatv),sse,spd,scd,rpl,drplde(ntens),drpldt,
     & time(2),dtime,temp,dtemp,predef(1),dpred(1),props(nprops),
     & coords(3),drot(3,3),pnewdt,celent,dfgrd0(3,3),dfgrd1(3,3)
	 
**: declare subroutine variables		 
      real(8) ss(ntens),sn(ntens),dsn(ntens),ddsdde(ntens,ntens),
     & ddsddt(ntens)
 
 
****************************************************************	 
**: declare parameter and other parameters 
****************************************************************
      integer i,j,k	  
	  
      real(8) yg,pn,sg	 
      real(8) sy,sy0,xk,xn 
      real(8) sq,sb	

	  integer mah
      parameter(mah = 3)
      real(8) zi(mah),ri(mah),ri0(mah),ri_delta(mah),di(mah)
      real(8) a_r,a_r0,a_s,a_w,a_m,a_w1  
	  real(8) b_r,b_m

      real(8) em(6,6),vem(6,6),tem(6,6)
      real(8) snp(6),dsnp(6),dp,pacc,Iso_Hard
      real(8) bhi(mah,6),ahi(mah,6),tah(6)
	  real(8) tbhi(mah,6),thi(mah),fbi(mah)
      real(8) tvs(6),vsa(6),fy
      real(8) pm(6,6),khm(6,6),ydp(6,6),dydp(6,6)
	  
      integer ite,itemax
      parameter (itemax=25)	  
      integer fakn
      real(8) odp,xakn(3)
      real(8) xx	 

      real(8) p_pre,w0

      real(8) Relax_time	  
	  

C-----  Material constants PROPS:
C
C       PROPS(1) - PROPS(2) -- elastic constants 
C           props(1): yg (young modulus)
C           props(2): pn (poisson ratio)
C     
C       PROPS(3) - PROPS(5) -- parameters for viscoplastic
C           props(3): sy0 (initial yield stress)
C           props(4): xn (parameter n)
C           props(5): xk (parameter K)
C
C       PROPS(6) - PROPS(7) -- isotropic parameters
C           props(6): sq (parameter Q)
C           props(7): sb (parameter b)
C
C       PROPS(8) - PROPS(17) -- kinematic parameters for linear hardening and dynamic recovery
C           props(8): Zi(1) (linear hardening parameter)
C           props(9): Zi(2) (linear hardening parameter)
C           props(10): Zi(3) (linear hardening parameter)
C           props(11): ri0(1) (initial static recovery parameter)
C           props(12): ri0(2) (initial static recovery parameter)
C           props(13): ri0(3) (initial static recovery parameter)
C           props(14): ri_delta(1) (total increase of static recovery parameter)
C           props(15): ri_delta(2) (total increase of static recovery parameter)
C           props(16): di(1) (increase speed of static recovery parameter)
C           props(17): di(2) (increase speed of static recovery parameter)
C
C       PROPS(18) - PROPS(22) -- kinematic parameters for static recovery
C           props(18): a_r0 (parameter r_0)
C           props(19): a_s (parameter phi_s)
C           props(20): a_w_1 (parameter omega_1)
C           props(21): a_w_2 (parameter omega_2)
C           props(22): a_m (parameter m)
C
C       PROPS(23) - PROPS(24) -- Isotropic parameters for static recovery
C           props(23): b_r (parameter γ_iso)
C           props(24): b_m (parameter m_iso)

	  
**:
************************************************************************
**: <Local variable definitions>
************************************************************************
**:
**: <elastic constants>
**：yg:杨氏模量；pn:泊松比；sg:剪切模量（拉梅第二参数μ）

	  yg = props(1)
	  pn = props(2)
	  sg = 0.5d0*yg/(1.0d0+pn)	  
	 
**:
**: <yield stress>
**：sy：总的屈服强度；sy0:初始屈服强度; xk:K, xn:n

      sy0 = props(3)
	  xn = props(4)
	  xk = props(5)		  
	  
**:
**: <Isotropic hardening parameters>	: No is this subroutine   
**  
      sq=props(6)	
	  sb=props(7)		  
	  
**:
**: <kinematic hardening parameters>
**   zi：线性硬化参数Ci; ri:动态恢复参数γi
     
	  zi(1) = props(8)
	  zi(2) = props(9)    
	  zi(3) = props(10) 
	  ri0(1) = props(11) 
	  ri0(2) = props(12) 	  
	  ri0(3) = props(13) 
	  ri_delta(1) = props(14) 
	  ri_delta(2) = props(15)   
	  di(1) = props(16) 
	  di(2) = props(17) 
	  
**   a_r,a_m: 随动强化中静态恢复参数	  
	  
	  a_r0=props(18) 
	  a_s=props(19) 
	  a_w=props(20) 
	  a_w1=props(21)	  
	  a_m = props(22) 
 
	  
**   b_r,b_m: 各向同性强化中静态恢复参数	  
	  
	  b_r=props(23) 
	  b_m=props(24) 
	     
	  	  
**:
***********************main routine start*******************************
**: <Labeling on state variables>
************************************************************************
**:
      k=1
      pacc=statev(k)
      k=k+1
	  
	  Iso_Hard = statev(k)
	  k=k+1
	  
      do j=1,ntens
         snp(j)=statev(k)
         k=k+1
      end do 
      do i=1,mah
         do j=1,ntens
            ahi(i,j)=statev(k)
            k=k+1
         end do
	  end do 
	  
	  Relax_time = statev(30)
	  
      if (abs(dsn(1)) < 1.0d-10) then
         Relax_time = Relax_time + dtime	  
  	  else
	     Relax_time = 0.0
	  endif	  

*** <put the expression of γi>
      do i=1,2
         ri(i)=ri0(i)+ri_delta(i)*(1.0d0-exp(-1*di(i)*pacc))
      end do    
      ri(3)=ri0(3)
		 
*** <put the expression of a_r>
       a_r=a_r0*(a_s+(1.0d0-a_s)*exp(-1.0d0*a_w1*pacc-1.0d0*a_w*Relax_time))
c	   write(6,*)"a_r = ", a_r

   
**:
************************************************************************
**: <Elastic stiffness>
************************************************************************
**:
      call kmkem( em,sg,pn,ndi,nshr )
      call kmkvem( vem,sg,ntens,ndi )
**:
************************************************************************
**: <Elastic predictor>
************************************************************************
**  sn：总应变张量；dsn：总应变率张量；snp:塑性应变张量
**  tvs: 预计偏应力张量; vem: 偏刚度张量; tah:预计背应力张量
**: <trial deviatoric stress>
      do i=1,ntens
         tvs(i)=0.0d0
         do j=1,ntens
            tvs(i)=tvs(i)+vem(i,j)*(sn(j)+dsn(j)-snp(j))
         end do
      end do 
**:
**: <trial back stress>
      do j=1,ntens
         tah(j)=0.0d0
         do i=1,mah
            tah(j)=tah(j)+ahi(i,j)
         end do
      end do 
	  
**:
************************************************************************
**: <Yield function>
************************************************************************
**:
      xx=0.0d0
      do j=1,ndi
         xx=xx+(tvs(j)-tah(j))**2
      end do 
      do j=ndi+1,ndi+nshr,1
         xx=xx+0.5d0*(tvs(j)-tah(j))**2
      end do 
      	  	  
      sy=sy0 + Iso_Hard
      fy=sqrt(1.5d0*xx)-1.0001d0*sy
**:
************************************************************************
**: <Plastic corrector>
************************************************************************
**:
      if (fy .gt. 0.0d0) then

         ite=1
         fakn=0
         odp=0.0d0
         do i=1,mah
            thi(i)=1.0d0
         end do
**:
**: <return mapping>
 1000    call krmap(dp,dsnp,vsa,tvs,ahi,thi,zi,sg,sy,sq,sb,
     &               xk,xn,mah,pacc,ndi,nshr,ntens,dtime)
	 
         call kaccel(dp,dsnp,xakn,fakn,ntens)
**:
**: <hardening parameters correction>
         call kkinh(thi,dsnp,dp,ri,a_r,a_m,
     &              mah,ntens,ndi,nshr,ahi,zi)	 	 	 
	 
         if ((abs(dp-odp).gt.1.0d-5*dp).and.(ite.lt.itemax)) then
            ite=ite+1
            odp=dp
            goto 1000
         endif	 
c         write(6,'("odp:  ",1d12.4)') odp
      endif
**:
************************************************************************
**: <internal variable update>
************************************************************************
**:
      if (fy .gt. 0.0d0) then
**： snp:塑性应变张量； dsnp:塑性应变率张量； pacc:累计塑性应变
         do j=1,ntens
            snp(j)=snp(j)+dsnp(j)
         end do
		 
         pacc=pacc+dp  
		 
		 Iso_Hard =Iso_Hard + sb*(sq - Iso_Hard)*dp - b_r*Iso_Hard**b_m*dtime

c         write(6,'("pacc:  ", 1d12.4)') pacc
         do j=1,ntens
            do i=1,mah
               ahi(i,j)=thi(i)*(ahi(i,j)+zi(i)*dsnp(j)/1.5d0)
            end do
         end do

      endif
**:
************************************************************************
**: <Stress update>
************************************************************************
**:
      do i=1,ntens
         ss(i)=0.0d0
         do j=1,ntens
            ss(i)=ss(i)+em(i,j)*(sn(j)+dsn(j)-snp(j))
         end do
      end do

**:
************************************************************************
**: <yield stress update>  Do not need in this subroutine
************************************************************************
**:  
       sy=sy0 + Iso_Hard  	   
C        write(6,*)"sy = " , sy 	
  
**:
************************************************************************
**: <Tangent operator for global Newton iteration>
************************************************************************
**:
      if (fy .gt. 0.0d0) then
         call kpm( pm,dydp,ydp,vsa,sy,dp,ntens,ndi,nshr,
     &		  pacc,p_pre,w0)
	  
         call kkhm( khm,zi,ri,thi,ahi,fbi,vsa,
     &              ntens,ndi,nshr,mah )
         call kctop(tem,pm,khm,vem,ntens )
      endif

      do i=1,ntens
         do j=1,ntens
            ddsdde(i,j)=em(i,j)
         end do
      end do
**:
************************************************************************
**: <State variables update>
************************************************************************
**:
      k=1
      statev(k)=pacc
      k=k+1

	  statev(k)=Iso_Hard
	  k=k+1
	  
      do j=1,ntens
         statev(k)=snp(j)
         k=k+1
      end do
	  
      do i=1,mah
         do j=1,ntens
            statev(k)=ahi(i,j)
            k=k+1
         end do
      end do


	  
	  statev(29) = dsn(1)
	  statev(30) = Relax_time	  
  
**:
*****************************************************main routine end***
**: <End of UMAT>
************************************************************************
**:
      return
      end



***************************************************************************************************************
**: <User-defined local functions and subroutines>*************************************************************
***************************************************************************************************************

***********************************************************************
**:     <Return mapping>          *************************************
***********************************************************************

      subroutine krmap( dp,dsnp,vsa,tvs,ahi,thi,zi,sg,sy,sq,sb,
     &                  xk,xn,mah,pacc,ndi,nshr,ntens,dtime)
	 
      implicit none
      integer mah,ndi,nshr,ntens
      real(8) dp,pacc,dsnp(6),vsa(6),tvs(6),dtime,dp1
      real(8) ahi(mah,6),thi(mah),zi(mah)
      real(8) sg,sy,sq,sb,xk,xn,yn,yn1


      integer i,j,ite,itemax
      real(8) tvsa(6),ahn(6),tsef,hh,fg,dfg
	  ite = 0
	  itemax = 25
	  

      do j=1,ntens
         ahn(j)=0.0d0
         do i=1,mah
            ahn(j)=ahn(j)+thi(i)*ahi(i,j)
         end do
      end do

      hh=0.0d0
      do i=1,mah
         hh=hh+thi(i)*zi(i)
      end do      
	   
      tsef=0.0d0
      do j=1,ndi
         tvsa(j)=tvs(j)-ahn(j)
         tsef=tsef+tvsa(j)**2
      end do  	  
      do j=ndi+1,ndi+nshr,1
         tvsa(j)=tvs(j)-ahn(j)
         tsef=tsef+0.5d0*tvsa(j)**2
      end do  
	  
      tsef=sqrt(tsef)	  

**:
**: newton-raphson method to solve dp; fg的表达式见word
**:

      dp=0.00d0
	  yn=sqrt(1.5d0)*tsef  

   80 do i=1,itemax
	     fg = yn-sqrt(1.5d0)*tsef+(3*sg+hh)*
     &	 ((yn-sy)/xk)**xn*dtime
        
         dfg = 1.0d0+(3*sg+hh)*xn*((yn-sy)
     &		     /xk)**(xn-1.0d0)*dtime/xk
		 
	     yn1=yn-fg/dfg				 
		 
		 if(abs(yn1-yn).lt. 1.0d-6*yn1) goto 20
		 yn=yn1
	     ite = ite +1
c		 write(6,*)"yn = ",yn
c		 write(6,*)"ite = ",ite	 
      end do 
	  
C      Write warning message to the .dat file
c      WRITE(6,*) "The implicit calculation did not converge!"

   20    continue
		 
	     dp1= ((yn-sy)/xk)**xn*dtime
	     if (abs(dp1-dp).gt. 1.0d-6*dp) then
	        dp = dp1
		    goto 80
         endif
	  
c        write(6,'("dp1:  ", 1d12.4)') dp1
c        write(6,'("yn:  ", 1d12.4)') yn	   
	  
	     dp = dp1
c		 write(6,*)"dp = ",dp	  
	  		 	 	 
	  	
cccc    vsa 为流动方向n; dsnp:塑性应变张量增量      		  
      do j=1,ntens
         vsa(j)=tvsa(j)/tsef
         dsnp(j)=sqrt(1.5d0)*dp*vsa(j)
      end do 

      end


***********************************************************************
**    Aitken's acceleration       *************************************
***********************************************************************

      subroutine kaccel(dp,dsnp,xakn,fakn,ntens)
      implicit none
      integer fakn,ntens
      real(8) dp,dsnp(6),xakn(3)
      integer j

      xakn(1)=xakn(2)
      xakn(2)=xakn(3)
      xakn(3)=dp

      if (fakn .ne. 3) then
         fakn=fakn+1
      elseif (abs( xakn(3)-xakn(2) ) .gt. 1.0d-10*dp) then
         dp=xakn(3)-(xakn(3)-xakn(2))
     &               /(1.0d0-(xakn(2)-xakn(1))/(xakn(3)-xakn(2)))
         if (dp .lt. 0.0d0) then
            dp=xakn(3)
         else
            do j=1,ntens
               dsnp(j)=dsnp(j)*dp/xakn(3)
            end do 
            xakn(3)=dp
            fakn=2
         endif
      endif	  
      end

***********************************************************************
**    <Kinematic hardening>       *************************************
***********************************************************************

      subroutine kkinh(thi,dsnp,dp,ri,a_r,a_m,
     &                  mah,ntens,ndi,nshr,ahi,zi)
      implicit none
      integer mah,ntens,ndi,nshr
      real(8) zi(mah),ri(mah),a_r,a_m
      real(8) ahi(mah,6),ahiNew(mah,6),thi(mah)
      real(8) dsnp(6),dp  

      integer i,j
      real(8) tsef_a(mah)


***********************************
**: Calcultae J2 of back stress ***
***********************************

**: update back stress 
		
      do j=1,ntens
         do i=1,mah
            ahiNew(i,j)=thi(i)*(ahi(i,j)+zi(i)*dsnp(j)/1.5d0)
         end do
      end do		
		
**: Calculte J2 of back stress 		
      do i=1,mah
         tsef_a(i)=0.0d0
		 
         do j=1,ndi
            tsef_a(i)=tsef_a(i)+ ahiNew(i,j)**2
         end do
		 
         do j=ndi+1,ndi+nshr,1
		    tsef_a(i)=tsef_a(i)+0.5d0*ahiNew(i,j)**2
         end do  
            
			tsef_a(i)=sqrt(1.5d0*tsef_a(i))
      end do    
   
c        write(6,*)"J2 of Back stress = ", tsef_a  


************************************
**: Updata variable thi        *****
************************************

      do i=1,mah
		  thi(i)= 1.0d0/(1.0d0+ri(i)*dp+a_r*tsef_a(i)**a_m)
      end do 

c      write(6,'("thi:  ", 1d12.4)') thi

      end



***********************************************************************
**: <Elastic stiffness matrix>       **********************************
***********************************************************************

      subroutine kmkem(em,sg,pn,ndi,nshr)
      implicit none
      integer ndi,nshr
      double precision em(6,6),sg,pn
 
      integer i,j
      double precision xx

      xx=2.0d0*sg/(1.0d0-2.0d0*pn)
      do i=1,ndi
         em(i,i)=(1.0d0-pn)*xx
         do j=i+1,ndi
            em(i,j)=pn*xx
            em(j,i)=em(i,j)
         end do 
      end do
	  
      do i=ndi+1,ndi+nshr,1
         em(i,i)=sg
         do j=i+1,ndi+nshr
            em(i,j)=0.0d0
            em(j,i)=em(i,j)
         end do 
      end do
	  
      do i=1,ndi
         do j=ndi+1,ndi+nshr,1
            em(i,j)=0.0d0
            em(j,i)=em(i,j)
         end do 
      end do

      end



***********************************************************************
**: <Deviatoric elastic stiffness matrix>   ***************************
***********************************************************************

      subroutine kmkvem(vem,sg,ntens,ndi)
      implicit none
      integer ntens,ndi
      double precision vem(6,6),sg

      integer j,k
      double precision xx

      xx=2.0d0*sg
      do j=1,ntens
         do k=1,ntens
            vem(j,k)=0.0d0
         end do 
         vem(j,j)=xx
      end do
	  
      xx=2.0d0*sg/3.0d0
      do j=1,ndi
         do k=1,ndi
            vem(j,k)=vem(j,k)-xx
         end do 
      end do

      end


***********************************************************************
**:  <Consistent tangent stiffness matrix>       **********************
***********************************************************************

      subroutine kctop(em,pm,khm,vem,ntens)
      implicit none
      integer ntens
      double precision em(6,6),pm(6,6),khm(6,6),vem(6,6)

      integer j,k
      double precision lm(6,6),lminv(6,6)
      double precision xm(6,6),ym(6,6)

      do j=1,ntens
         do k=1,ntens
            lm(j,k)=vem(j,k)+pm(j,k)+khm(j,k)
         end do 
      end do
c         write(6,'("lm:  ", 1d12.4)') lm    

      call kminv( lm,lminv,ntens )
c	     write(6,'("lminv:  ", 1d12.4)') lminv
      call kmprod( lminv,vem,ym,ntens )
c	     write(6,'("ym:  ", 1d12.4)') ym
      call kmprod( em,ym,xm,ntens )
c	     write(6,'("xm:  ", 1d12.4)') xm

      do j=1,ntens
         do k=1,ntens
            em(j,k)=em(j,k)-xm(j,k)
         end do 
      end do

      end



***********************************************************************
**:   <Kinematic hardening tangent operator>     **********************
***********************************************************************

      subroutine kkhm( khm,zi,ri,thi,bhi,fbi,vsa,
     &                 ntens,ndi,nshr,mah )
      implicit none
      integer ntens,ndi,nshr,mah
      double precision khm(6,6),zi(mah),ri(mah),thi(mah)
      double precision bhi(mah,6),fbi(mah)
      double precision vsa(6)

      integer i,j,k
      double precision xx

      xx=0.0d0
      do i=1,mah
         xx=xx+ri(i)*thi(i)*zi(i)/1.5d0
      end do 
	  
      do j=1,ntens
         do k=1,ntens
            khm(j,k)=0.0d0
         end do 
         khm(j,j)=xx
      end do 

      do i=1,mah
         xx=ri(i)*thi(i)*zi(i)
         do j=1,ntens
            do k=1,ndi
               khm(j,k)=khm(j,k)-xx*bhi(i,j)*bhi(i,k)
            end do 			   
            do k=ndi+1,ndi+nshr,1
               khm(j,k)=khm(j,k)-0.5d0*xx*bhi(i,j)*bhi(i,k)
            end do
         end do
      end do
	  
c         write(6,'("khm:  ", 1d12.4)') khm
      end



***********************************************************************
**:  <Perfectly plastic tangent operator>        **********************
***********************************************************************

      subroutine kpm(pm,dydp,ydp,vsa,sy,dp,ntens,ndi,nshr,
     &	        pacc,p_pre,w0)
      implicit none
      integer ntens,ndi,nshr
      double precision pm(6,6),dydp(6,6),ydp(6,6),vsa(6),sy,dp
	  double precision pacc,value_dydp,p_pre,w0

      integer j,k

      do j=1,ntens
         do k=1,ndi
            ydp(j,k)=-sy/(1.5d0*(dp+1.0d-12))*vsa(j)*vsa(k)
         end do
         do k=ndi+1,ndi+nshr,1
            ydp(j,k)=-0.5d0*sy/(1.5d0*(dp+1.0d-12))*vsa(j)*vsa(k)
         end do
         ydp(j,j)=ydp(j,j)+sy/(1.5d0*(dp+1.0d-12))
      end do

**:  note: pacc cannot be zero 
      value_dydp = 0.0d0
      do j=1,ntens
	     do k=1,ndi
		    dydp(j,k)=value_dydp/(1.5d0)*vsa(j)*vsa(k)
         end do
         do k=ndi+1,ndi+nshr,1
		    dydp(j,k)=0.5d0*value_dydp/(1.5d0)*vsa(j)*vsa(k)
         end do
      end do  
	
      do j=1,ntens
         do k=1,ntens
            pm(j,k)=dydp(j,k)+ydp(j,k)
         end do
      end do	
c         write(6,'("pm:  ", 1d12.4)') pm     

      end



***********************************************************************
**:    <Inversion of matricies>      **********************************
***********************************************************************

      subroutine kminv( am,invm,nsize )
      implicit none
      integer nsize
      double precision am(6,6),invm(6,6)

      integer i,j,k
      double precision xx,xm(6,6)

      do i=1,nsize
         do j=1,nsize
            xm(i,j)=am(i,j)
            invm(i,j)=0.0d0
         end do
         invm(i,i)=1.0d0
      end do

      do k=1,nsize-1
         do j=k+1,nsize
            xx=xm(k,j)/xm(k,k)
            do i=k+1,nsize
               xm(i,j)=xm(i,j)-xm(i,k)*xx
            end do
			
            do i=1,nsize
               invm(i,j)=invm(i,j)-invm(i,k)*xx
            end do
         end do
      end do

      do k=1,nsize
         do j=nsize,1,-1
            xx=invm(k,j)
            do i=j+1,nsize
               xx=xx-xm(i,j)*invm(k,i)
            end do
            invm(k,j)=xx/xm(j,j)
         end do
      end do

      end


***********************************************************************
**:    <Matrix production>           **********************************
***********************************************************************

      subroutine kmprod( am,bm,cm,ntens )
      implicit none
      integer ntens
      double precision am(6,6),bm(6,6),cm(6,6)

      integer i,j,k

      do j=1,ntens
         do k=1,ntens
            cm(j,k)=0.0d0
            do i=1,ntens
               cm(j,k)=cm(j,k)+am(j,i)*bm(i,k)
            end do
         end do
      end do

      end

***************************************************************bottom***
